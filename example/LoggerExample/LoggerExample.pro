QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

SOURCES += \
        main.cpp

INCLUDEPATH += \
    $$PWD/../../src/ \

LIBS += -L$$PWD/../../lib/ -lLogger


#unix:!android{
#   QMAKE_CXXFLAGS+=-fsanitize=address -fno-omit-frame-pointer
#   QMAKE_LFLAGS+=-fsanitize=address
#}
