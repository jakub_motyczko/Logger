#include <QCoreApplication>
#include <QDateTime>

#include "Logger.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MOTiCO::Logger::instance()->install(MOTiCO::Logger::File | MOTiCO::Logger::StdOut,
                                        QString("log_%1.txt").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm")));

    INFO << "This is an info message";
    DEBUG << "This is a debug message";
    WARNING << "This is a warning message";
    ERROR << "This is an error message";

    return 0;
}
