/*
 * Copyright (C) MOTiCO All Rights Reserved
 *
 * ** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 *
 * Written by Jakub Motyczko <jakub@motyczko.pl>
 */

#include "Logger.h"

#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QFileInfo>
#include <QThread>
#include <iostream>

using namespace std;

namespace MOTiCO {

QString Logger::m_fileName;
Logger::Mode Logger::m_mode = Logger::Mode::StdOut;

Logger::Logger()
{

}

bool Logger::install(uint mode, const QString &fileName)
{
    if (mode > (File | StdOut)) {
        ERROR << "Unexpected mode";
        return false;
    }
    m_fileName = QString("%1/%2").arg(qApp->applicationDirPath()).arg(fileName);
    m_mode = (Mode)mode;

    if (m_mode & Mode::File)
    {
        QFile file(m_fileName);
        if (!file.open(QFile::WriteOnly | QFile::Append))
        {
            DEBUG << "Could not create log file:" << m_fileName << file.errorString();
            return false;
        }
        file.close();
    }

    qInstallMessageHandler(Logger::customMessageHandler);

    if (m_mode & Mode::File)
        std::cout << "\n\n\n================================================================================\n\n\n";

    return true;
}

Logger *Logger::instance()
{
    static Logger *_instance = new Logger;
    return _instance;
}

void Logger::customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(context);

    if (m_mode & Mode::File && !m_fileName.isEmpty())
    {
        QString message = modifyMessage(Mode::File, type, context, msg);

        QFile outFile(m_fileName);
        outFile.open(QIODevice::WriteOnly | QIODevice::Append);

        QTextStream textStream(&outFile);
        textStream << message;
        outFile.close();

        if (m_mode & Mode::StdOut == 0) {
            std::cout << message.toUtf8().data();
            flush(std::cout);
        }
    }

    if (m_mode & Mode::StdOut)
    {
        QString message = modifyMessage(Mode::StdOut, type, context, msg);
        std::cout << message.toUtf8().data();
        flush(std::cout);
    }
}

QString Logger::modifyMessage(Mode mode, QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    bool colorize = mode == Mode::StdOut;
    QString messageTypeSymbol, messageStartColor, messageEndColor = "\e[0m", contextStartColor = "\e[37m", contextEndColor = "\e[0m";
    switch (type)
    {
    case QtDebugMsg:
        messageTypeSymbol = QStringLiteral("[D]");
        messageStartColor = QStringLiteral("");
        break;
    case QtWarningMsg:
        messageTypeSymbol = QStringLiteral("[W]");
        messageStartColor = QStringLiteral("\e[30m\e[43m");
        break;
    case QtCriticalMsg:
        messageTypeSymbol = QStringLiteral("[C]");
        messageStartColor = QStringLiteral("\e[41m");
        break;
    case QtFatalMsg:
        messageTypeSymbol = QStringLiteral("[F]");
        messageStartColor = QStringLiteral("\e[41m");
        break;
    case QtInfoMsg:
        messageTypeSymbol = QStringLiteral("[I]");
        messageStartColor = QStringLiteral("\e[44m");
        break;
    }

    QString message = msg;
    message.prepend(messageTypeSymbol);
    if (colorize && !messageStartColor.isEmpty()) {
        message.prepend(messageStartColor);
        message.append(messageEndColor);
    }
    //show message under the source info with 3 tabs in front
    message.replace("\n", "\n\t\t\t");

    const QString dateTime = QDateTime::currentDateTime().toString("dd-MM-yyyy hh:mm:ss  ");
    QString contextString = QString("%1%2\t%7(%3)\t%4:%5\t%6").
            arg(QString(context.category) != "default" ? QString("%1 | ").arg(context.category) : "").
            arg(dateTime).
            arg(QString::number(reinterpret_cast<intptr_t>(QThread::currentThread()), 16)).
            arg(QFileInfo(context.file).fileName()).
            arg(context.line).
            arg(context.function).
            arg(QThread::currentThread()->objectName());

    if (colorize && !contextStartColor.isEmpty()) {
        contextString.prepend(contextStartColor);
        contextString.append(contextEndColor);
    }

    return QString("%1\n\t\t\t%2\n").arg(contextString, message);
}

}
