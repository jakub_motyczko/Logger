/*
 * Copyright (C) MOTiCO All Rights Reserved
 *
 * ** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 *
 * Written by Jakub Motyczko <jakub@motyczko.pl>
 */

/*
 * Example usage:
 *
 * MOTiCO::Logger::instance()->install(MOTiCO::Logger::File | MOTiCO::Logger::StdOut,
                                        QString("log_%1.txt").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm")));
 */

#ifndef LOGGER_H
#define LOGGER_H

#include <QString>

#include "logger_global.h"

namespace MOTiCO {

class LOGGERSHARED_EXPORT Logger
{
public:
    enum Mode
    {
        StdOut = 0x1,
        File = 0x2,
    };
    Q_DECLARE_FLAGS(Modes, Mode)

    /*!
     * \brief Returns an instance of the Logger
     * Usually it's needed only for initialization purpose
     * \return
     */
    static Logger *instance();

    /*!
     * \brief Initializes the Logger with provided mode and filename
     * \param mode - modes that this Logger should operate with
     * \param fileName - log file that application should write into
     *
     * \return returns false if fileName couldn't be created or if the mode is incorrect
     */
    bool install(uint mode, const QString &fileName = QString());

private:
    Logger();

    static Mode m_mode;
    static QString m_fileName;

    static void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

    static QString modifyMessage(Mode mode, QtMsgType type, const QMessageLogContext &context, const QString &msg);
};

}

#include <QDebug>

#undef DEBUG
#undef ERROR
#undef WARNING

#ifndef INFO
    #define INFO qInfo().noquote()
#endif

#ifndef DEBUG
    #ifdef QT_DEBUG
        #define DEBUG qDebug().noquote()
    #else
        #define DEBUG qDebug().noquote() << Q_FUNC_INFO
    #endif
#endif

#ifndef WARNING
    #ifdef QT_DEBUG
        #define WARNING qWarning().noquote()
    #else
        #define WARNING qWarning().noquote() << Q_FUNC_INFO
    #endif
#endif

#ifndef ERROR
    #define ERROR qCritical().noquote()
#endif


#endif // LOGGER_H
